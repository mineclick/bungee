package net.mineclick.bungee.service;

import lombok.Getter;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.ReconnectHandler;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.mineclick.bungee.MCBungee;
import net.mineclick.bungee.model.ServerModel;
import net.mineclick.bungee.model.ServerStatus;
import net.mineclick.bungee.util.SingletonInit;

import java.net.InetSocketAddress;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Getter
@SingletonInit
public class ServersService implements ReconnectHandler {
    private static ServersService i;

    private final Set<ServerModel> servers = ConcurrentHashMap.newKeySet();

    private ServersService() {
        ProxyServer.getInstance().getScheduler().schedule(MCBungee.i(), () -> {
            // Expire servers after 10 seconds of no updates
            Instant now = Instant.now();
            servers.stream()
                    .filter(s -> s.getExpiresAt().isBefore(now))
                    .forEach(this::remove);
        }, 1, 1, TimeUnit.SECONDS);

        ProxyServer.getInstance().setReconnectHandler(this);

        ProxyServer.getInstance().getServers().remove("lobby");
    }

    public static ServersService i() {
        return i == null ? i = new ServersService() : i;
    }

    /**
     * Handles a server's status update
     *
     * @param server The {@link ServerModel}
     */
    public void handle(ServerModel server) {
        if (server.getStatus().equals(ServerStatus.RUNNING)
                || server.getStatus().equals(ServerStatus.WAITING_TO_RESTART)) {
            add(server);
        } else {
            remove(server);
        }
    }

    private void add(ServerModel server) {
        if (!servers.remove(server)) {
            String id = server.getId();
            ServerInfo info = ProxyServer.getInstance().constructServerInfo(
                    id,
                    InetSocketAddress.createUnresolved(id, 25565),
                    "MineClick server - " + id,
                    false
            );
            ProxyServer.getInstance().getServers().put(id, info);
            MCBungee.i().getLogger().info("Server added: " + server.getId());
        }

        servers.add(server);
    }

    private void remove(ServerModel server) {
        servers.remove(server);

        ProxyServer.getInstance().getServers().remove(server.getId());
        MCBungee.i().getLogger().info("Server removed: " + server.getId());
    }

    private ServerInfo getBestServer() {
        List<ServerModel> collect = servers.stream()
                .filter(s -> s.getStatus().equals(ServerStatus.RUNNING) && s.isGame())
                .collect(Collectors.toList());
        if (collect.isEmpty()) {
            collect = new ArrayList<>(servers);
        }

        return collect.stream()
                .map(s -> ProxyServer.getInstance().getServers().get(s.getId()))
                .min(Comparator.comparingInt(i -> {
                    int size = i.getPlayers().size();
                    return size > 10 ? size : -size;
                })).orElse(null);
    }

    @Override
    public ServerInfo getServer(ProxiedPlayer player) {
        ServerInfo server = getBestServer();
        if (server == null) {
            player.disconnect(new ComponentBuilder("MineClick server is restarting, please try again").color(ChatColor.RED).create());
            return null;
        }

        return server;
    }

    @Override
    public void setServer(ProxiedPlayer player) {
    }

    @Override
    public void save() {
    }

    @Override
    public void close() {
    }
}
