package net.mineclick.bungee.service;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.scheduler.ScheduledTask;
import net.md_5.bungee.event.EventHandler;
import net.mineclick.bungee.MCBungee;
import net.mineclick.bungee.messenger.OnlinePlayersHandler;
import net.mineclick.bungee.util.SingletonInit;
import net.mineclick.core.messenger.Action;

import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@SingletonInit
public class ConnectionService implements Listener {
    private static ConnectionService i;

    private ConnectionService() {
        ProxyServer.getInstance().getPluginManager().registerListener(MCBungee.i(), this);

        // double check online players on startup
        AtomicReference<ScheduledTask> schedule = new AtomicReference<>();
        schedule.set(ProxyServer.getInstance().getScheduler().schedule(MCBungee.i(), () -> {
            OnlinePlayersHandler handler = new OnlinePlayersHandler();
            handler.setResponseConsumer(message -> {
                if (message == null || !message.isOk()) return;
                schedule.get().cancel();

                Set<String> onlinePlayers = ((OnlinePlayersHandler) message).getPlayers().stream()
                        .map(OnlinePlayersHandler.UUIDWrapper::getUuid).collect(Collectors.toSet());
                Set<String> proxiedPlayers = ProxyServer.getInstance().getPlayers().stream()
                        .map(ProxiedPlayer::getUniqueId).map(UUID::toString).collect(Collectors.toSet());

                onlinePlayers.removeIf(proxiedPlayers::contains);
                if (!onlinePlayers.isEmpty()) {
                    OnlinePlayersHandler deleteHandler = new OnlinePlayersHandler();
                    for (String uuid : onlinePlayers) {
                        deleteHandler.getPlayers().add(new OnlinePlayersHandler.UUIDWrapper(uuid));
                        MCBungee.i().getLogger().info("Purged a zombie online player: " + uuid);
                    }
                    deleteHandler.send(Action.DELETE);
                }
            });
            handler.send(Action.GET);
        }, 10, 30, TimeUnit.SECONDS));
    }

    public static ConnectionService i() {
        return i == null ? i = new ConnectionService() : i;
    }

    @EventHandler
    public void on(PlayerDisconnectEvent e) {
        // Give it a second to make sure the global's onQuit doesn't override this
        ProxyServer.getInstance().getScheduler().schedule(MCBungee.i(), () -> {
            OnlinePlayersHandler handler = new OnlinePlayersHandler();
            handler.getPlayers().add(
                    new OnlinePlayersHandler.UUIDWrapper(e.getPlayer().getUniqueId().toString())
            );
            handler.send(Action.DELETE);
        }, 1, TimeUnit.SECONDS);
    }

    @EventHandler
    public void on(PostLoginEvent e) {
        int minVersion = SettingsService.i().getMinVersion();
        if (e.getPlayer().getPendingConnection().getVersion() < minVersion) {
            ComponentBuilder builder = new ComponentBuilder("You are joining with an older version of Minecraft.\n" +
                    "It is recommended to use the latest version to avoid glitches and bugs.")
                    .color(ChatColor.RED);
            e.getPlayer().sendMessage(builder.create());
        }

        e.getPlayer().setTabHeader(new ComponentBuilder(ChatColor.GOLD + " Welcome to " + ChatColor.YELLOW + "MineClick! \n ").create(),
                new ComponentBuilder("\n" + ChatColor.DARK_AQUA + "Website: " + ChatColor.AQUA + "mineclick.net").create());
    }
}
