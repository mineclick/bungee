package net.mineclick.bungee.service;

import lombok.Getter;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.chat.*;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.scheduler.ScheduledTask;
import net.md_5.bungee.event.EventHandler;
import net.mineclick.bungee.MCBungee;
import net.mineclick.bungee.messenger.SettingsHandler;
import net.mineclick.bungee.model.AnnouncementsModel;
import net.mineclick.bungee.model.MotdModel;
import net.mineclick.bungee.util.SingletonInit;
import net.mineclick.bungee.util.Strings;
import net.mineclick.core.messenger.Action;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Getter
@SingletonInit
public class SettingsService implements Listener {
    private static final Pattern linkPattern = Pattern.compile("\\[(.+)\\]\\((.+)\\)");
    private static SettingsService i;

    private TextComponent motdText = new TextComponent("Loading...");
    private AnnouncementsModel announcements = new AnnouncementsModel();
    private int minVersion = 735;
    private ScheduledTask task;
    private int announcementIndex;

    private SettingsService() {
        ProxyServer.getInstance().getPluginManager().registerListener(MCBungee.i(), this);

        AtomicReference<ScheduledTask> schedule = new AtomicReference<>();
        schedule.set(ProxyServer.getInstance().getScheduler().schedule(MCBungee.i(), () -> {
            SettingsHandler handler = new SettingsHandler();
            handler.setResponseConsumer(message -> {
                if (message != null) {
                    update(((SettingsHandler) message).getMotd(), ((SettingsHandler) message).getAnnouncements());
                    schedule.get().cancel();
                } else {
                    MCBungee.i().getLogger().severe("Configurations cannot be loaded!");
                }
            });
            handler.send(Action.GET);
        }, 0, 10, TimeUnit.SECONDS));
    }

    public static SettingsService i() {
        return i == null ? i = new SettingsService() : i;
    }

    public void update(MotdModel motdModel, AnnouncementsModel announcementsModel) {
        MCBungee.i().getLogger().info("Updating configurations");

        // update min version
        minVersion = motdModel.getMinVersion();

        updateMotd(motdModel);
        this.announcements = announcementsModel;

        announcementIndex = 0;
        if (task != null) {
            task.cancel();
        }

        if (announcements == null || announcements.getAnnouncements().length == 0)
            return;

        task = ProxyServer.getInstance().getScheduler().schedule(MCBungee.i(), () -> {
            if (announcementIndex >= announcements.getAnnouncements().length)
                announcementIndex = 0;

            String msg = ChatColor.translateAlternateColorCodes('&', announcements.getAnnouncements()[announcementIndex++]);
            TextComponent output = new TextComponent(line());

            for (String line : msg.split("\\\\[n]")) {
                TextComponent builder = new TextComponent();
                builder.setColor(ChatColor.YELLOW);


                Matcher linkMatcher = linkPattern.matcher(line);
                while (linkMatcher.find()) {
                    int index = line.indexOf(linkMatcher.group());
                    builder.addExtra(line.substring(0, index));
                    line = line.substring(index + linkMatcher.group().length());

                    String linkName = linkMatcher.group(1);
                    String linkUrl = linkMatcher.group(2);
                    TextComponent link = new TextComponent(linkName);
                    link.setColor(ChatColor.AQUA);
                    link.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Click to open " + linkUrl).color(ChatColor.DARK_AQUA).create()));
                    link.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, linkUrl));
                    builder.addExtra(link);
                }
                builder.addExtra(line);

                String spaces = Strings.appendSpaces("", (Strings.CHAT_LENGTH / 2) - (Strings.length(builder.toPlainText()) / 2));
                output.addExtra(new ComponentBuilder("\n" + spaces).create()[0]);
                output.addExtra(builder);
            }

            output.addExtra("\n");
            output.addExtra(line());
            ProxyServer.getInstance().getPlayers().forEach(p -> p.sendMessage(output));
        }, 1, announcements.getInterval(), TimeUnit.MINUTES);
    }

    private BaseComponent line() {
        return new ComponentBuilder("---------------------------------------------").color(ChatColor.GRAY).bold(true).strikethrough(true).create()[0];
    }

    private void updateMotd(MotdModel motd) {
        if (motd == null) {
            return;
        }

        String top = motd.getTop() == null ? "&6MineClick" : motd.getTop();
        top += "&r";
        String bottom = motd.getBottom() == null ? "" : motd.getBottom();

        if (motd.isTopCentered()) {
            top = Strings.center(top, Strings.MOTD_LENGTH);
        }
        if (motd.isBottomCentered()) {
            bottom = Strings.center(bottom, Strings.MOTD_LENGTH);
        }

        String appendix = motd.getTopAppendix();
        if (appendix != null && !appendix.isEmpty()) {
            int topLength = Strings.length(top);
            appendix = Strings.end(motd.getTopAppendix(), Strings.MOTD_LENGTH - topLength);
            top += appendix;
        }

        ComponentBuilder builder = new ComponentBuilder();
        parseColor(builder, top);
        builder.append("\n");
        parseColor(builder, bottom);

        motdText = new TextComponent(builder.create());
    }

    private void parseColor(ComponentBuilder builder, String string) {
        String[] split = string.split("#");
        for (int i = 0, splitLength = split.length; i < splitLength; i++) {
            String part = split[i];
            if (i == 0) {
                builder.append(ChatColor.translateAlternateColorCodes('&', part));
            } else {
                String color = "#" + part.substring(0, 6);
                part = part.substring(6);
                builder.append(new ComponentBuilder(ChatColor.translateAlternateColorCodes('&', part)).color(ChatColor.of(color)).create());
            }
        }
    }

    @EventHandler
    public void on(ProxyPingEvent e) {
        ServerPing ping = e.getResponse();
        ping.setDescriptionComponent(motdText);
        e.setResponse(ping);
    }
}
