package net.mineclick.bungee.service;

import com.vexsoftware.votifier.bungee.events.VotifierEvent;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.mineclick.bungee.MCBungee;
import net.mineclick.bungee.messenger.VoteHandler;
import net.mineclick.bungee.util.SingletonInit;
import net.mineclick.core.messenger.Action;

@SingletonInit
public class VotesService implements Listener {
    private static VotesService i;

    private VotesService() {
        ProxyServer.getInstance().getPluginManager().registerListener(MCBungee.i(), this);
    }

    public static VotesService i() {
        return i == null ? i = new VotesService() : i;
    }

    @EventHandler
    public void on(VotifierEvent e) {
        String username = e.getVote().getUsername();
        String serviceName = e.getVote().getServiceName();
        if (username == null || serviceName == null)
            return;

        VoteHandler voteHandler = new VoteHandler();
        voteHandler.setUsername(username);
        voteHandler.setServiceName(serviceName);

        // first send it as an update to the game servers
        voteHandler.setResponseConsumer(message -> {
            if (message == null || !message.isOk()) {
                // If no game server was able to process it, send it to ender
                VoteHandler postHandler = new VoteHandler();
                postHandler.setUsername(username);
                postHandler.setServiceName(serviceName);
                postHandler.send(Action.POST);
            }
        });
        voteHandler.send(Action.UPDATE);
    }
}
