package net.mineclick.bungee.model;

import lombok.Data;

@Data
public class AnnouncementsModel {
    private String[] announcements = new String[0];
    private int interval = 0;
}
