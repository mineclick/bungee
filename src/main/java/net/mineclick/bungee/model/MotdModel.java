package net.mineclick.bungee.model;

import lombok.Data;

@Data
public class MotdModel {
    private String top = "MineClick";
    private String bottom = "loading...";
    private String topAppendix = "";
    private int minVersion = 735; // 1.16
    private boolean topCentered;
    private boolean bottomCentered;
}
