package net.mineclick.bungee.model;

public enum ServerStatus {
    STARTING,
    RUNNING,
    WAITING_TO_RESTART,
    RESTARTING
}
