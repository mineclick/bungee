package net.mineclick.bungee.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ServerModel {
    private final transient Instant expiresAt = Instant.now().plus(10, ChronoUnit.SECONDS);

    @EqualsAndHashCode.Include
    private String id;
    private ServerStatus status;
    private int players;
    private int cpuLoad;
    private double ramUsage;
    private boolean game;
    private Instant startedOn;
    private Instant restartsOn;
}
