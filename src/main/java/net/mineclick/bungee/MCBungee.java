package net.mineclick.bungee;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Getter;
import net.md_5.bungee.api.plugin.Plugin;
import net.mineclick.bungee.util.SingletonInitializer;
import net.mineclick.core.messenger.Messenger;
import net.mineclick.core.messenger.redisWrapper.JedisWrapper;

@Getter
public class MCBungee extends Plugin {
    private static MCBungee i;

    private final Gson gson = new GsonBuilder()
            .excludeFieldsWithoutExposeAnnotation()
            .create();

    public static MCBungee i() {
        return i;
    }

    @Override
    public void onEnable() {
        i = this;

        new Messenger("bungee", new JedisWrapper(Constants.REDIS_HOST, Constants.REDIS_PORT, Constants.REDIS_PASSWORD), getLogger());
        SingletonInitializer.loadAll();

        getLogger().info("MCBungee loaded");
    }
}
