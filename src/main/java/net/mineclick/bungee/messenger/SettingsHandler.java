package net.mineclick.bungee.messenger;

import lombok.Getter;
import net.mineclick.bungee.model.AnnouncementsModel;
import net.mineclick.bungee.model.MotdModel;
import net.mineclick.bungee.service.SettingsService;
import net.mineclick.core.messenger.Action;
import net.mineclick.core.messenger.Message;
import net.mineclick.core.messenger.MessageName;

@Getter
@MessageName("settings")
public class SettingsHandler extends Message {
    private MotdModel motd;
    private AnnouncementsModel announcements;

    @Override
    public void onReceive() {
        if (getAction().equals(Action.UPDATE)) {
            SettingsService.i().update(motd, announcements);
        }
    }
}
