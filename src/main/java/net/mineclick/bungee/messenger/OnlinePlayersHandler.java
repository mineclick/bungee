package net.mineclick.bungee.messenger;

import lombok.Getter;
import lombok.Value;
import net.mineclick.core.messenger.Message;
import net.mineclick.core.messenger.MessageName;

import java.util.ArrayList;
import java.util.List;

@MessageName("onlinePlayers")
public class OnlinePlayersHandler extends Message {
    @Getter
    private final List<UUIDWrapper> players = new ArrayList<>();

    @Value
    public static class UUIDWrapper {
        String uuid;
    }
}
