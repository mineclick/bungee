package net.mineclick.bungee.messenger;

import lombok.Setter;
import net.mineclick.core.messenger.Message;
import net.mineclick.core.messenger.MessageName;

@Setter
@MessageName("votes")
public class VoteHandler extends Message {
    private String username;
    private String serviceName;
}
