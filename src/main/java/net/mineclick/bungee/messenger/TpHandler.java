package net.mineclick.bungee.messenger;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.mineclick.core.messenger.Message;
import net.mineclick.core.messenger.MessageName;
import net.mineclick.core.messenger.Response;

import java.util.UUID;

@MessageName("tp")
public class TpHandler extends Message {
    private UUID uuid;
    private String serverId;

    @Override
    public void onReceive() {
        ProxiedPlayer player = ProxyServer.getInstance().getPlayer(uuid);
        Response response = Response.NOT_FOUND;

        if (player != null && serverId != null) {
            ServerInfo info = ProxyServer.getInstance().getServerInfo(serverId);
            if (info != null) {
                player.connect(info);

                response = Response.OK;
            }
        }

        send(response);
    }
}
