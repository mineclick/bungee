package net.mineclick.bungee.messenger;

import net.mineclick.bungee.model.ServerModel;
import net.mineclick.bungee.service.ServersService;
import net.mineclick.core.messenger.Message;
import net.mineclick.core.messenger.MessageName;

@MessageName("servers")
public class ServersHandler extends Message {
    private ServerModel server;

    @Override
    public void onReceive() {
        ServersService.i().handle(server);
    }
}
