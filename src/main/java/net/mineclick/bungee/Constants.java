package net.mineclick.bungee;

public class Constants {
    public static final boolean PROD = Boolean.parseBoolean(System.getenv("PROD"));
    public static final String REDIS_HOST = PROD ? "redis" : "localhost";
    public static final int REDIS_PORT = 6379;
    public static final String REDIS_PASSWORD = null;
}
