package net.mineclick.bungee.util;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface SingletonInit {
    String instanceMethod() default "i";
}
