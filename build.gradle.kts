plugins {
    `java-library`
    `maven-publish`
    alias(libs.plugins.com.github.johnrengelman.shadow)
}

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}

repositories {
    mavenLocal()
    mavenCentral()
    maven {
        name = "papermc-maven"
        url = uri("https://repo.papermc.io/repository/maven-public/")
    }
    maven {
        name = "gitlab-maven"
        url = uri("https://gitlab.com/api/v4/groups/5826166/-/packages/maven")
    }
}

dependencies {
    implementation(libs.net.mineclick.core.messenger)
    implementation(libs.redis.clients.jedis)
    testImplementation(libs.junit.junit)
    compileOnly(libs.io.github.waterfallmc.waterfall.api)
    compileOnly(libs.com.vexsoftware.nuvotifier.bungeecord)
    compileOnly(libs.org.projectlombok.lombok)
    annotationProcessor(libs.org.projectlombok.lombok)
}

tasks {
    withType<JavaCompile> {
        options.encoding = "UTF-8"
    }

    shadowJar {
        archiveFileName.set("MCBungee.jar")
    }
}

publishing {
    publications.create<MavenPublication>("maven") {
        from(components["java"])
    }
    repositories {
        maven {
            name = "gitlab-maven"
            url = uri("https://gitlab.com/api/v4/projects/13759900/packages/maven")

            //val tokenProperty = findProperty("mineclickGitlabToken") as String?
            val ciJobToken: String? = System.getenv("CI_JOB_TOKEN")

            //(tokenProperty ?: ciJobToken)?.let {
            ciJobToken?.let {
                credentials(HttpHeaderCredentials::class) {
                    name = "Job-Token"
                    value = it
                }
                authentication {
                    create<HttpHeaderAuthentication>("header")
                }
            }
        }
    }
}